package webCrawler

import java.util.HashSet
import ch.ethz.dal.tinyir.processing.Tokenizer
import scala.collection.mutable.MutableList
import scala.math.min
import java.security.MessageDigest

class Detector
{
  // variables recording results
  var duplicates = 0
  var duplicates1 = 0
  var duplicates2 = 0
  var duplicatesrest = 0
  
  // variables for hashing, etc.
  val distinctPages = new MutableList[(Array[Int], String)]()
  val firstDuplicatePages = new MutableList[(Array[Int])]()
  
  // function to print results
  def result {
    println("Exact duplicates found: " + duplicates)
    println("1/128 duplicates found: " + duplicates1)
    println("2/128 duplicates found: " + duplicates2)
    //println(duplicatesrest + " dist other pages found.")
  }
  
  def processContent(name: String, html: String): Boolean = {
    val tokens = Tokenizer.tokenize(html)

    //Hash the content using sim-hash and save the value
    val hashValue = simHash(tokens)
    val hashTuple = (hashValue, name)
    var h = 200 // just has to be larger than max hamming dist
    distinctPages.foreach(a => h = min(hammingDistance(a._1, hashValue),h))
    
    if (h == 0){
      if(!firstDuplicatePages.contains(h))
        duplicates +=1
        firstDuplicatePages += hashValue
    }
    else{
      if (h == 1) duplicates1 +=1
      if (h == 2) duplicates2 +=1
      if (h > 2) duplicatesrest += 1
      distinctPages += hashTuple
    }
    //println(h + "hammingDistance")
      
    return (h == 0)
  }

  def md5(str: String) = {
    MessageDigest.getInstance("MD5").digest(str.getBytes)
  }
  
  def simHash(tokens: List[String]): Array[Int] = {
    val shingles = tokens.sliding(3).toList
    val shingleHashes = shingles.map(a => md5(a.mkString))
    val intHashes = shingleHashes.map(a => getIntHash(a))
    var docHash = new Array[Int](128)
    intHashes.foreach(a => {docHash = sumArrayInts(docHash, a)} )
    docHash = correctDocHash(docHash)
    return docHash
  }
  
  //convert positive ints to 1 and negative ints to 0
  def correctDocHash(docHashUnc: Array[Int]): Array[Int] = {
    val docHash = new Array[Int](128)
    var i = 0
    for(i <- 0 until 128){
      if (docHashUnc(i) > 0) docHash(i) = 1 else docHash(i) == 0
    }
    return docHash
  }
  
  //convert byte array to 128 length array of ints with 1 and -1
  def getIntHash(binHash: Array[Byte]): Array[Int] = {
    val intHash = new Array[Int](128)
    var i = 0
    var idx = 127
    for(i <- 0 until binHash.length){
      var j = 0
      for(j <- 0 until 8){
        if ( ((binHash(15 - i) >> j) & 1) == 1){
          intHash(idx) = 1
        }
        else{
          intHash(idx) = -1
        }
        idx -= 1
      }
    }
    return intHash
  }
  
  //sum two 128 int arrays
  def sumArrayInts(a: Array[Int], b: Array[Int]): Array[Int] = {
    val sumHash = new Array[Int](128)
    var i = 0
    for(i <- 0 until 128){
      sumHash(i) = a(i) + b(i)
    }
    return sumHash
  }

  /*def hammingDistance(a: Int, b: Int): Int = {
    var bitOr = a^b;
    var count = 0
    while (bitOr != 0){
      bitOr = bitOr & (bitOr-1)
      count += 1
    }
    return count
  }*/
    
  def hammingDistance(a: Array[Int], b: Array[Int]): Int = {
    var count = 0
    var i = 0
    for (i <- 0 until 128){
      if (a(i) != b(i)) count += 1
    }
    return count
  }
  
  def binary(value: Int) : String = 
    String.format("%32", Integer
        .toBinaryString(value))
        .replace(' ', '0')

}