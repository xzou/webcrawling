package webCrawler

//import java.io._

class Counter
{
  private var pageCount = 0
  private var englishCount = 0
  private var studentCount = 0
  
  def result {
    println("Number of pages found: " + pageCount)
    println("Term frequency of student: " + studentCount )
    println("Unique English pages found: " + englishCount)
  }
  
  def count (url: String, html: String) {
    countStudent(html)
    countEnglish(url)
    pageCount = pageCount + 1
  }
  
  private def countStudent (html: String) {
    studentCount += "student".r.findAllIn(html).length
  }
  
  private def countEnglish (url: String) {
    if(url.contains("/en") || url.contains("index_en")) 
      englishCount = englishCount + 1
  }
}