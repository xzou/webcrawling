package webCrawler

import java.util.Date
import java.text.SimpleDateFormat

object Main {
  def main(args: Array[String]) = 
  {
    val crawler = new Crawler(args(0))
    crawler.crawl
    crawler.result
    val sdf= new SimpleDateFormat("yyyy-mm-dd hh:mm:ss")
    //println(sdf.format(new Date()))
  }
}